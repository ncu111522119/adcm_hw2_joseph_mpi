# Hellow MPI

# 目的
測試MPI Hello world

# 檔案目錄
* hello.c: MPI 程式碼
* output_helloworld: MPI 編譯後的檔案

# 指令操作
## 下載程式碼
```
$ wget http://pdc2.csie.ncu.edu.tw/ADCM/mpiexamples/hello.c
```

##  程式碼執行
### 執行編譯
```
## mpicc {filename} -o {output_filename}
$ mpicc ./hello.c -o output_helloworld
```
### 執行編譯檔案
```
# 輸出結果
## 有4個 Process
$ mpiexec -np 4 ./output_helloworld
Hello from process 2 of 4
Hello from process 0 of 4
Hello from process 3 of 4
Hello from process 1 of 4

```

# 思考
* 建立 Process
* 如何知道 Process 的順序
* 