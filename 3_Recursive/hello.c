#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>


// Define Josephus Problem function
// n = numbers of process
// k = every k process to kill a process
// rank = current first process
int josephus(int n, int k, int rank) {
    // if only one process, return 0 
    if (n == 1)
        return rank;
    else {
        int next_rank = (rank + 1) % n;
        int survivor = josephus(n - 1, k, next_rank);
        if (survivor < k - 1)
            return survivor;
        else
            return survivor + 1;
    }
}

int main(int argc, char** argv) {
    int rank, size;
    int n = 4;  // Number of process
    int k = 4;   // Elimination factor
    int result;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank); // get process id
    MPI_Comm_size(MPI_COMM_WORLD, &size); // get how many Process

    // printf("rank:%d -1 \n", rank);

    // check if process number smaller than process created by MPI
    if (size < n) {
        if (rank == 0)
            printf("Number of processes should be greater than or equal to n.\n");
        MPI_Finalize(); // stop the process
        return 0;
    }

    // printf("rank:%d -2 \n", rank);

    //  using MPI_Barrier to ensure that all processes start the computation at the same time.
    //  MPI_Barrier will wait the program finish before the MPI_Barrier being called.
    MPI_Barrier(MPI_COMM_WORLD);

    // printf("rank:%d -3 \n", rank);

    result = josephus(n, k, rank);

    //  using MPI_Barrier to ensure that all processes start the computation at the same time
    //  MPI_Barrier will wait the program finish before the MPI_Barrier being called.
    MPI_Barrier(MPI_COMM_WORLD);

    // printf("rank:%d -4 \n", rank);

    if (rank == result)
        printf("Process %d is the survivor.\n", rank);
        MPI_Finalize();
        return rank;

    MPI_Finalize(); // stop the process
    return 0;
}