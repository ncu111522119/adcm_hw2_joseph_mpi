# Linked list use c code

# 目的
練習撰寫 C code 的 LinkedList 

# 執行方式
## 編譯 .c file
```
$gcc -o linkedlist linkedlist.c
```

## 執行編譯檔案
```
$ ./linkedlist
1   2   3
```

# 參考資料
* [What is Linked List](https://www.geeksforgeeks.org/what-is-linked-list/)
    * [C 語言動態記憶體配置教學：malloc、free 等函數](https://blog.gtwang.org/programming/c-memory-functions-malloc-free/)
    * [Linked List 連結串列 - 是自行設定 node 的數量](https://codimd.mcl.math.ncu.edu.tw/s/B1rd5-sM4)
* [使用 gcc 編譯程式](https://shaochien.gitbooks.io/how-to-use-gcc-to-develop-c-cpp-on-windows/content/use-gcc-to-compile-program.html)
