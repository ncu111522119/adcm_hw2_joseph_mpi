// A simple C program for
// traversal of a linked list

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int data;          // 預計存放的資料
    struct node *next; // 下一個Node 的記憶體位址
};

typedef struct node Node;

// This function prints contents of linked list starting
// from the given node
void printList(Node *n)
{
    while (n != NULL)
    {
        printf("address= %p ", n);
        printf("=> data= %d \n", n->data); // 列印 data 資料
        n = n->next;             // 不斷到下一個 Node
    }
}

// Driver's code
int main()
{
    // process 數量
    int n = 10;
    int i = 0;
    // 定義 前一個、目前、後一個 Node
    Node *head, *current, *previous;

    for (i = 1; i <= n; i++)
    {
        current = (Node *)malloc(sizeof(Node));
        current -> data = i;
        if (i == 1)
        {
            head = current;
            previous = current;
        } else {
            previous -> next = current;
            current -> next = NULL;
            previous = current;

        }
    }

    printList(head);

    return 0;
}