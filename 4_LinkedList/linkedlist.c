// A simple C program for
// traversal of a linked list
  
#include <stdio.h>
#include <stdlib.h>
  
struct Node {
    int data; // 預計存放的資料
    struct Node* next; // 下一個Node 的記憶體位址
};
  
// This function prints contents of linked list starting
// from the given node
void printList(struct Node* n)
{
    while (n != NULL) {
        printf(" %d ", n->data); // 列印 data 資料
        n = n->next; //不斷到下一個 Node
    }
}
  
// Driver's code
int main()
{
    struct Node* head = NULL;
    struct Node* second = NULL;
    struct Node* third = NULL;
  
    // allocate 3 nodes in the heap
    /// malloc : 配置該Node 的記憶體大小
    head = (struct Node*)malloc(sizeof(struct Node));
    second = (struct Node*)malloc(sizeof(struct Node));
    third = (struct Node*)malloc(sizeof(struct Node));
  
    head->data = 1; // assign data in first node
    head->next = second; // Link first node with second
  
    second->data = 2; // assign data to second node
    second->next = third;
  
    third->data = 3; // assign data to third node
    third->next = NULL;
  
    // Function call
    printList(head);
  
    return 0;
}