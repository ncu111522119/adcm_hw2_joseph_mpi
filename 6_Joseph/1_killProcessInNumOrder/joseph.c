#include <stdio.h>
#include <mpi.h>

int main(int argc, char *argv[])
{

    /* my process ID and total number of zxc*/
    int myrank, nprocs;
    int turn_kill = 3;
    int turn_kill_index = 3;
    int myNumbers[] = {1, 2, 3, 4};
    int proccess_num = 4;
    /* Create child processes, each of which has its own variables.
     * From this point on, every process executes a separate copy
     * of this program.  Each process has a different process ID,
     * ranging from 0 to num_procs minus 1, and COPIES of all
     * variables defined in the program. No variables are shared.
     **/
    MPI_Init(&argc, &argv);

    /* find out MY process ID, and how many processes were started. */
    // Process 的第幾個
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    // 目前 Prcess 的數量
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    
    if (myrank == 0)
    {
        /* Rank 0 sends an integer to each of the other process ranks */
        int i;
        int value = 0;
        for (i = 1; i < nprocs; i++)
        {
            value = value + i;
            MPI_Status status;
            printf("Process %d send ",myrank);
            MPI_Send(&value, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
            MPI_Recv(&value, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
        }
    }
    else
    {

        /* All other process ranks receive one number from Rank 0 */
        int value;
        MPI_Status status;
        MPI_Recv(&value, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
        printf("Rank %d received value %d\n", myrank, value);
    }

    printf("Hello from process %d of %d\n", myrank, nprocs);

    /* Stop this process */
    // Kill
    MPI_Finalize();
    return 0;
}
