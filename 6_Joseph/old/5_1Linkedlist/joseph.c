#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>

struct node
{
    int data;          // 預計存放的資料
    struct node *next; // 下一個Node 的記憶體位址
};

typedef struct node Node;

// This function prints contents of linked list starting
// from the given node
void printList(Node *n)
{
    while (n != NULL)
    {
        printf("address= %p ", n);
        printf("=> data= %d \n", n->data); // 列印 data 資料
        n = n->next;             // 不斷到下一個 Node
    }
}

// Node SetLinkedList(int nprocs, Node *head){
//     // 定義 前一個、目前、後一個 Node
//     Node *current, *previous;
//     // 根據 n 個 proc 設置 LinkedList
//     for (int i = 0; i < nprocs; i++)
//     {
        
//         current = (Node *)malloc(sizeof(Node));
//         current->data = i;
//         if (i == 0)
//         {
//             head = current;
//             previous = current;
//         }
//         else
//         {
//             previous->next = current;
//             current->next = NULL;
//             previous = current;
//         }
//     }
// }


int main(int argc, char *argv[])
{

    /* my process ID and total number of zxc*/
    int myrank, nprocs;
    Node *head, *current, *previous;

    
    /* Create child processes, each of which has its own variables.
     * From this point on, every process executes a separate copy
     * of this program.  Each process has a different process ID,
     * ranging from 0 to num_procs minus 1, and COPIES of all
     * variables defined in the program. No variables are shared.
     **/
    MPI_Init(&argc, &argv);

    /* find out MY process ID, and how many processes were started. */
    // Process 的第幾個
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    // 目前 Prcess 的數量
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    // 根據 n 個 proc 設置 LinkedList
    for (int i = 0; i < nprocs; i++)
    {
        if (i == myrank)
        {
            printf("Hello World from process %d of %d\n", myrank, nprocs);
            current = (Node *)malloc(sizeof(Node));
            current->data = myrank;
            if (myrank == 0)
            {
                head = current;
                previous = current;
            }
            else
            {
                previous->next = current;
                current->next = NULL;
                previous = current;
            }
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }

    // printList(head);

    // MPI_Barrier(MPI_COMM_WORLD);
    /* Stop this process */
    // Kill
    MPI_Finalize();
    return 0;
}
