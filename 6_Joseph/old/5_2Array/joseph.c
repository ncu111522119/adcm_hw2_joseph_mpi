#include <stdio.h>
#include <mpi.h>



int main(int argc, char *argv[])
{

    /* my process ID and total number of zxc*/
    int myrank, nprocs;
    int turn_kill = 3;
    int turn_kill_index = 3;
    int procArray[4];
    int proccess_num = 4;
    /* Create child processes, each of which has its own variables.
     * From this point on, every process executes a separate copy
     * of this program.  Each process has a different process ID,
     * ranging from 0 to num_procs minus 1, and COPIES of all
     * variables defined in the program. No variables are shared.
     **/
    MPI_Init(&argc, &argv);

    // while (1)
    // {
    //     printf("hi\n");
    //     for (int index = 0; index < nprocs; index++)
    //     {
    //         // 跳過已經死掉的，讓 Index 累加
    //         while (procArray[index] == -1)
    //         {
    //             index += 1;
    //             if (procArray[index] != -1)
    //             {
    //                 break;
    //             }
    //         }
            
    //         if(nprocs == 1){
    //             printf("finall: Hello from process %d of %d\n", myrank, nprocs);
    //             return 0;
    //         }else if(turn_kill_index == 1){
    //             // kill process
    //             procArray[index] = -1;
    //             // Process 數量減一
    //             proccess_num -= 1;
    //             //重新設定 turn_kill 之值
    //             turn_kill_index = turn_kill + 1;
    //             printf("Hello from process %d of %d\n", myrank, nprocs);
    //             // print("index:",index,"|",procArray);
    //         }
    //     }
    // }

    /* find out MY process ID, and how many processes were started. */
    // Process 的第幾個
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    // 目前 Prcess 的數量
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    printf("Hello from process %d of %d\n", myrank, nprocs);

    /* Stop this process */
    // Kill
    MPI_Finalize();
    return 0;
}
