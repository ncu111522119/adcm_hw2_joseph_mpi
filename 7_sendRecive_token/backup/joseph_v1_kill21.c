#include <stdio.h>
#include <mpi.h>
#include <math.h>

int main(int argc, char *argv[])
{

    int myrank, nprocs;
    int token = 1;
    MPI_Status status;
    int k = 3;
    int thisToken;
    int whoIsKilled[] = {1, 1, 1, 1};

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    if (myrank == 0)
    {
        // ======= Send Section ===========
        thisToken = token;
        printf("Process %d's token is %d\n", myrank, thisToken);
        int dest = myrank + 1;
        // printf("Process %d's dest: %d\n", myrank, dest);
        if (dest > nprocs)
        {
            dest = 0;
        }
        token += 1;
        MPI_Send(&token, 1, MPI_INT, dest, 0, MPI_COMM_WORLD);
        // printf("===p0 is finish===\n");
    }
    while (thisToken != k)
    {
        // 若Token 達到輪尋數量時，退出回圈

        // 調整要收的編號，來自哪個寄件者
        int source = myrank - 1;
        if (source < 0)
        {
            source = nprocs - 1;
        }

        // printf("I'm %d, I am waiting for Process %d.\n",myrank , source);
        MPI_Recv(&token, 1, MPI_INT, source, 0, MPI_COMM_WORLD, &status);
        // ======= Send Section ===========
        thisToken = token;
        // if(myrank == 0)
        //     printf("Process %d's token is %d from Process %d \n", myrank, thisToken, source);
        int dest = myrank + 1;
        // 調整收件者編號
        if (dest == nprocs)
        {
            dest = 0;
        }

        // printf("Process %d's dest: %d\n", myrank, dest);
        token += 1;
        MPI_Send(&token, 1, MPI_INT, dest, 0, MPI_COMM_WORLD);
    }

    // ======= Send Section ===========
    thisToken = token;
    int dest = myrank + 1;
    if (dest == nprocs)
    {
        dest = 0;
    }
    token = 1;
    MPI_Send(&token, 1, MPI_INT, dest, 0, MPI_COMM_WORLD);
    printf("=> Finish Single from process %d of %d\n", myrank, nprocs);
    // printf("This is Process %d,\n", myrank);
    // for (int i = 0; i < nprocs; i++)
    // {
    //     printf("%d IsKilled? %d \n", i, whoIsKilled[i]);
    // }
    MPI_Finalize();
    return 0;
}
