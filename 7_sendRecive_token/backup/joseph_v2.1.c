#include <stdio.h>
#include <mpi.h>
#include <math.h>

int main(int argc, char *argv[])
{

    int myrank, nprocs;
    int token = 1;
    MPI_Status status;
    int k = 3;
    int thisToken;
    int whoIsKilled[] = {1, 1, 1, 1};
    int finalKilled = 0;
    int killed = 1;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    if (myrank == 0)
    {
        // ======= Send Section ===========
        thisToken = token;
        printf("Process %d's token is %d\n", myrank, thisToken);
        int dest = myrank + 1;
        // printf("Process %d's dest: %d\n", myrank, dest);
        if (dest > nprocs)
        {
            dest = 0;
        }
        token += 1;
        printf("1. MPI_Send: %d -> %d\n", myrank, dest);
        MPI_Send(&token, 1, MPI_INT, dest, 0, MPI_COMM_WORLD);
        // printf("===p0 is finish===\n");
    }
    
    

    while (1)
    {
        MPI_Barrier(MPI_COMM_WORLD);
        // 若Token 達到輪尋數量時，退出回圈
        if (thisToken == k)
        {
            thisToken = token;
            int dest = myrank + 1;
            // 超過時，更新目的地
            if (dest == nprocs)
            {
                dest = 0;
            }
            token = 1;
            killed = 1; // this process is killed;
            MPI_Send(&killed, 1, MPI_INT, finalKilled, 0, MPI_COMM_WORLD);
            printf("3. MPI_Send: %d -> %d\n", myrank, dest);
            MPI_Send(&token, 1, MPI_INT, dest, 0, MPI_COMM_WORLD);
            break;
        }
        else
        {
            MPI_Recv(&whoIsKilled, 1, MPI_INT, finalKilled, 0, MPI_COMM_WORLD, &status);
            // for (int i = 0; i < nprocs; i++)
            // {
            //     printf("%d live? 【%d】 =>", i, whoIsKilled[i]);
            // }
            // printf("^ process%d \n", myrank);
            killed = 0; // this process is not killed;
            MPI_Send(&killed, 1, MPI_INT, finalKilled, 0, MPI_COMM_WORLD);
            // 希望可以達到更新陣列狀態
        }

        // ======= Recive Section ===========
        // 調整要收的編號，來自哪個寄件者
        int source = myrank - 1;
        if (source < 0)
        {
            source = nprocs - 1;
        }
        // 跳過 已經死掉的 Process ID
        while (whoIsKilled[source] == 0)
        {
            if (source < 0)
            {
                source = nprocs - 1;
            }
            else
            {
                source -= 1;
            }
            // printf("Process %d is killed.", source);
            if (whoIsKilled[source] == 1)
            {
                break;
            }
        }
        // printf("I'm %d, I am waiting for Process %d.\n",myrank , source);
        MPI_Recv(&token, 1, MPI_INT, source, 0, MPI_COMM_WORLD, &status);

        // ======= Send Section ===========
        thisToken = token;
        int dest = myrank + 1;
        // 調整收件者編號
        if (dest == nprocs)
        {
            dest = 0;
        }

        // 跳過 已經死掉的 Process ID
        while (whoIsKilled[dest] == 0)
        {
            if (dest == nprocs)
            {
                dest = 0;
            }
            else
            {
                dest += 1;
            }
            // printf("Process %d is killed.", dest);
            if (whoIsKilled[dest] == 1)
            {
                break;
            }
        }
        if (thisToken != k)
        {
            token += 1;
            printf("2. MPI_Send: %d -> %d\n", myrank, dest);
            MPI_Send(&token, 1, MPI_INT, dest, 0, MPI_COMM_WORLD);
        }
    }

    // ======= Send Section ===========

    printf("=> Finish Single from process %d of %d\n", myrank, nprocs);
    // printf("This is Process %d,\n", myrank);
    // for (int i = 0; i < nprocs; i++)
    // {
    //     printf("%d IsKilled? %d \n", i, whoIsKilled[i]);
    // }
    MPI_Finalize();
    return 0;
}
