#include <stdio.h>
#include <mpi.h>
#include <math.h>

int sendNextOne(int myrank, int nprocs, int *whoIsLive)
{

    int dest = myrank + 1;
    if (dest == nprocs)
    {
        dest = 0;
    }

    // 跳過 已經死掉的 Process ID
    while (whoIsLive[dest] == 0)
    {
        if (dest == nprocs)
        {
            dest = 0;
        }
        else
        {
            dest += 1;
        }
        // printf("Process %d is killed.", dest);
        if (whoIsLive[dest] == 1)
        {
            break;
        }
    }
    return dest;
}

int procToken(int token, int k)
{
    if (token == k)
    {
        token = 1;
    }
    else
    {
        token += 1;
    }

    return token;
}

int recvPreviousOne(int myrank, int nprocs, int *whoIsLive)
{

    int source = myrank - 1;
    if (source < 0)
    {
        source = nprocs - 1;
    }
    // 跳過 已經死掉的 Process ID
    while (whoIsLive[source] == 0)
    {
        if (source <= 0)
        {
            source = nprocs - 1;
        }
        else
        {
            source -= 1;
        }
        // printf("Process %d is killed.", source);
        if (whoIsLive[source] == 1)
        {
            break;
        }
    }
    return source;
}

int main(int argc, char *argv[])
{
    int *sendPtr;
    int myrank, nprocs;
    int token = 1;
    int dest;
    MPI_Status status;
    int k = 3;
    int thisToken;
    int whoIsKilled = -1;
    int whoIsLive[] = {1, 1, 1, 1};
    int killed = -1;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    if (myrank == 0)
    {
        // ======= Send Section ===========
        thisToken = token;
        token = procToken(token, k);
        dest = sendNextOne(myrank, nprocs, whoIsLive);
        MPI_Send(&token, 1, MPI_INT, dest, 0, MPI_COMM_WORLD);
        // printf(" %d = [%d] => %d\n", myrank, token, dest);
        // printf("===p0 is finish===\n");
    }
    // 若Token 達到輪尋數量時，退出回圈thisToken != k)
    int j = 1;
    while (j < 9)
    {
        // ======= update Who is killed ==========
        // MPI_Barrier(MPI_COMM_WORLD);
        // printf("==start===\n");
        for (int i = 0; i < nprocs; i++)
        {
            if (killed != -1)
            {
                whoIsKilled = myrank;
            }
            MPI_Bcast(&whoIsKilled, 1, MPI_INT, i, MPI_COMM_WORLD);
            // MPI_Barrier(MPI_COMM_WORLD);
            if (whoIsKilled >= 0)
            {
                whoIsLive[whoIsKilled] = 0;
            }
        }
        ///// 查看某個 proc 的存活陣列
        if (myrank == 0)
        {
            for (int i = 0; i < nprocs; i++)
            {
                printf("[%d]:%d > ", i, whoIsLive[i]);
            }
            printf("At Process %d \n", myrank);
        }
        // if (myrank == 3)
        // {
        //     // printf("%d???\n", killed);
        // }
        if (killed == -1)
        {
            // ====== Recv Section ===========
            // 調整要收的編號，來自哪個寄件者
            int source = recvPreviousOne(myrank, nprocs, whoIsLive);
            int liveCount = 0;
            for (int i = 0; i < nprocs; i++)
            {
                if (whoIsLive[i] == 1)
                {
                    liveCount += 1;
                }
            }

            // if(liveCount > 2){
            // printf("is2: %d",is2);
            printf(" %d = [] => %d(%d)\n", source, myrank, token);
            MPI_Recv(&token, 1, MPI_INT, source, 0, MPI_COMM_WORLD, &status);
            // }

            thisToken = token;
            if (thisToken == k)
            {
                killed = myrank;
                whoIsLive[myrank] = 0;
                // printf("\n%d is bye!\n",myrank);
                // printf("=> Finish Single from process %d of %d\n", myrank, nprocs);
            }

            // ======= Send Section ==========
            token = procToken(token, k);
            dest = sendNextOne(myrank, nprocs, whoIsLive);
            MPI_Send(&token, 1, MPI_INT, dest, 0, MPI_COMM_WORLD);
            printf(" %d = [%d] => %d\n", myrank, token, dest);
        }

        j++;
    }

    // printf("=> Finish Single from process %d of %d\n", myrank, nprocs);
    MPI_Finalize();
    return 0;
}
