#include <stdio.h>
#include <mpi.h>
#include <math.h>

int main(int argc, char *argv[])
{

    /* my process ID and total number of zxc*/
    int myrank, nprocs;
    int whoIsKilled = -1;
    int killed = -1;
    int killedArray[] = {0, 0, 0, 0};
    /* Create child processes, each of which has its own variables.
     * From this point on, every process executes a separate copy
     * of this program.  Each process has a different process ID,
     * ranging from 0 to num_procs minus 1, and COPIES of all
     * variables defined in the program. No variables are shared.
     **/
    MPI_Init(&argc, &argv);

    /* find out MY process ID, and how many processes were started. */
    // Process 的第幾個
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    // 目前 Prcess 的數量
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    for (int i = 0; i < 3; i++)
    {
        if (myrank == 2 & i == 2)
        {
            killed = 2;
            killedArray[myrank] = 1;
            break;
        }
        else if (myrank == 3 & i == 1)
        {
            killed = 3;
            killedArray[myrank] = 1;
            break;
        }
    }

    MPI_Barrier(MPI_COMM_WORLD);
    for (int i = 0; i < nprocs; i++)
    {
        if(killed != -1){
            whoIsKilled = myrank;
        }
        MPI_Bcast(&whoIsKilled, 1, MPI_INT, i, MPI_COMM_WORLD);
        // MPI_Barrier(MPI_COMM_WORLD);
        // MPI_Barrier(MPI_COMM_WORLD);
        if (myrank == 0)
        {
            printf("Process %d , %d is killed \n", myrank, whoIsKilled);
        }
        if (whoIsKilled>0)
        {
            killedArray[whoIsKilled] = 1;
        }
    }

    // printf("Hello from process %d of %d\n", myrank, nprocs);

    if (myrank == 1)
    {
        printf("Process %d", myrank);
        for (int i = 0; i < nprocs; i++)
        {
            printf("[%d]:%d > ", i, killedArray[i]);
        }
    }

    /* Stop this process */
    // Kill
    MPI_Finalize();
    return 0;
}