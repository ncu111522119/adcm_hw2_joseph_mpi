# 

# 執行指令
```
$ mpicc ./joseph.c -o output_joseph
$ mpiexec -np 4 ./output_joseph
```
## OR
```
$ sh joseph.sh
```

# 相關程式碼

```
MPI_Send(void* buf, int count, MPI_Datatype 數據類型, int dest, int tag, MPI_Comm comm)
MPI_Recv(void* buf, int count, MPI_Datatype 數據類型, int source, int tag, MPI_Comm comm, MPI_Status *status)
````=


# 參考資料
* [MPI簡介](https://carleton.ca/rcs/rcdc/introduction-to-mpi/)