#include <stdio.h>
#include <mpi.h>


int *josephArray(int nprocs, int k)
{
    // printf("k=%d\n",k);
    int thisN = nprocs;         // 紀錄Procs 的數量，主要用於記錄更動狀態
    int token = 1;              // 自殺 Token
    int liveArray[4];           // 存活狀況
    int i = 0;                  // <liveArray的序號> 輪詢 Index
    static int killedArray[4];  // 死亡名單
    int j = 0;                  // <killedArray的序號>暫存每回死亡名單的序號

    // 初始 liveArray 的存活狀態，1 代表存活、-1 代表死亡
    for (int i = 0; i < nprocs; i++)
    {
        liveArray[i] = 1;
    }

    while (1)
    {
        // 存活剩餘最後一位
        if (thisN == 1)
        {
            // 添加到約瑟夫自殺順序中的陣列
            killedArray[j] = i;
            break;
        }
        // 達到 k ，需要殺掉
        if (token == k)
        {
            liveArray[i] = -1;  // Process 被殺掉，狀態為 -1，代表死亡
            killedArray[j] = i; // 將死亡 proc 的序號紀錄於死亡名單中
            token = 0;          // 重新設定 自殺 token 之值
            thisN--;            // Process 數量減一
            j++;
        }
        // 更新輪詢 index
        i++;
        /// 若超過 nprocs 之值，則更為0，使程序持續輪詢
        if (i >= nprocs)
        {
            i = 0;
        }
        // 跳過已經死掉的陣列
        while (liveArray[i] == -1)
        {
            i++;
            if (i >= nprocs)
            {
                i = 0;
            }
            if (liveArray[i] == 1)
            {
                break;
            }
        }
        // 自殺 token
        token++;
    }
    // printf("[");
    // for(int i = 0; i < nprocs ;i++){
    //     printf("%d, ", killedArray[i]);
    // }
    // printf("]\n");
    // 回傳自殺名單
    return killedArray;
}

int main(int argc, char *argv[])
{
    // 變數宣告
    int myrank, nprocs;
    // 第幾個需要自殺淘汰
    int k = 4;
    MPI_Init(&argc, &argv);
    // Process 的第幾個
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    // 目前 Prcess 的數量
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Status status;
    MPI_Request request;
    int i = 0;
    int token;
    int dest;
    int source;
                          
    // 取得約瑟夫的自殺順序陣列
    int *killedArray = josephArray(nprocs, k); 
    // 等待所有程序抵達，意味已經算完約瑟夫的自殺順序陣列
    MPI_Barrier(MPI_COMM_WORLD);

    // 執行ＭＰＩ程序關閉流程
    if (myrank == killedArray[0]) // 於自殺陣列中的第一位
    {
        // 取得預計發送的 Process ID
        int dest = killedArray[i + 1];
        token = 1;
        printf("Killed P %d, send(%d)\n", myrank, dest);
        // 非同步發送，將 Token 寄送給 自殺陣列中的下一位
        MPI_Isend(&token, 1, MPI_INT, dest, 0, MPI_COMM_WORLD, &request);
        // 關閉 此 process
        MPI_Finalize();
        return 0;
    }
    else if (myrank == killedArray[nprocs - 1])// 於自殺陣列中的最後一位
    {
        // 得到最後一位的 Array index
        i = nprocs - 1;
        // 預計接收的 Process ID
        int source = killedArray[i - 1];
        // 標準接收，接收自殺陣列中的前一位 Token
        MPI_Recv(&token, 1, MPI_INT, source, 0, MPI_COMM_WORLD, &status);
        printf("Killed P %d, Rec(%d)\n", myrank, source);
        // 關閉 此 process
        MPI_Finalize();
        return 0;
    }
    else // 非第一位、最後一位 
    {
        for (i = 1; i < nprocs - 1; i++)
        {
            // 取得 目前Process 的 自殺陣列Index
            if (myrank == killedArray[i])
            {
                // 預計接收的 Process ID
                int source = killedArray[i - 1];
                // 取得預計發送的 Process ID
                int dest = killedArray[i + 1];
                // 接收自殺陣列中的前一位 Token
                MPI_Recv(&token, 1, MPI_INT, source, 0, MPI_COMM_WORLD, &status);
                printf("Killed P %d, Rec(%d)&send(%d)\n", myrank, source, dest);
                // 非同步發送，將 Token 寄送給 自殺陣列中的下一位
                MPI_Isend(&token, 1, MPI_INT, dest, 0, MPI_COMM_WORLD, &request);
                MPI_Finalize();
                return 0;
            }
        }
    }
}
